from util.data_function import *


if __name__ == "__main__":
    list_of_exlcude_word = ["et", "ou", "de", "le", "la", "du", "des", "les", "en"
                            "of", "the", "at", "for", "and"]

    dict_true_model = map_json_data("list_job_tech_dataScience_YES.json", list_of_exlcude_word)
    dict_true_model = reduce_dict(dict_true_model)

    dict_false_model = map_json_data("list_job_tech_dataScience_NO.json", list_of_exlcude_word)
    dict_false_model = reduce_dict(dict_false_model)

    list_data_to_test = get_data_to_test("list_job_to_test.json")

    dict_job_result = is_job_data_scientist(list_data_to_test, dict_true_model, dict_false_model)
    with open('output_resultat.json', 'w') as outfile:
        json.dump(dict_job_result, outfile, indent=4)
