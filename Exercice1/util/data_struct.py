class JobStruct:

    def __init__(self, categorie="None", job_title="None", company_name="None", contrat_type="None",
                 is_data_scientist=False):
        self.categorie = categorie
        self.job_title = job_title
        self.company_name = company_name
        self.contrat_type = contrat_type
        self.is_data_scientist = is_data_scientist
