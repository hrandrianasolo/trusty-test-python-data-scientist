import json

from util.data_struct import JobStruct
from util.util import *


def map_json_data(filename: str, expression_to_exclude: list = []) -> list:
    """
    This method will map all word data from a json file
    :param filename: name of file to map
    :param expression_to_exclude: list of expression to exclude
    :return: list of dictionary. key:'word' and key:'count'
    :rtype: list
    """
    dict = []

    with open(filename) as json_file:
        data = json.load(json_file)
        for raw_data in data:
            job_title = normalize_string(str(raw_data['jobTitle']))
            job_title = job_title.split(' ')

            for job in job_title:
                if len(job) > 1 and job not in expression_to_exclude:
                    tmp = {
                        'word': job,
                        'count': 1
                    }
                    dict.append(tmp)
                else:
                    pass

    return dict


def reduce_dict(dict_mapped: list) -> dict:
    """
    This method will reduce mapped dictionary and set number of the word repeated
    :type dict_mapped: list of mapped dictionary
    :return dictionary of word count
    """
    dict_reduced = {}

    for data in dict_mapped:
        current_word = data['word']
        current_value = data['count']

        if current_word in dict_reduced:
            dict_reduced[current_word]['count'] = dict_reduced[current_word]['count'] + current_value
        else:
            dict_reduced[current_word] = {}
            dict_reduced[current_word]['count'] = current_value

    return dict_reduced


def get_data_to_test(filename):
    data = []

    file = open(filename, 'r')
    for line in file.readlines():
        raw_data = line.split(',')
        for tmp in raw_data:
            if len(tmp) > 2:
                data.append(normalize_string(tmp).rstrip())

    return data


def is_job_data_scientist(list_data_to_test: list, dict_true_model: dict, dict_false_model: dict) -> list:
    """
    This method will compare every word of job to test with
    our false model dictionary and true model dictionary of Data scientist
    And then create a native scoring data to tell if this job is Data scientist
    or not
    :param list_data_to_test:
    :param dict_true_model:
    :param dict_false_model:
    :rtype: list: list of JobStruct
    """
    job_processed = []

    for job in list_data_to_test:
        data_splited = job.lstrip().split(' ')
        is_data_scientist = 0

        for word in data_splited:
            if word in dict_true_model and word in dict_false_model:
                true_word_count = dict_true_model[word]['count']
                false_word_count = dict_false_model[word]['count']

                if true_word_count > false_word_count:
                    is_data_scientist += get_pourcentage(true_word_count, false_word_count)
                elif true_word_count < false_word_count:
                    is_data_scientist -= get_pourcentage(false_word_count, true_word_count)
                else:
                    pass

            elif word in dict_true_model:
                is_data_scientist += 0.3
            elif word in dict_false_model:
                is_data_scientist -= 0.3
            else:
                pass

        if is_data_scientist > 0:
            job_processed.append({"categorie": "Tech",
                                            "jobTitle": job.lstrip(),
                                            "companyName": "",
                                            "contratType": "",
                                            "is_data_scientist": True
                                            })

        else:
            job_processed.append({"categorie": "Tech",
                                            "jobTitle": job.lstrip(),
                                            "companyName": "",
                                            "contratType": "",
                                            "is_data_scientist": False
                                            })

    return job_processed
