import unicodedata
import re


def normalize_string(text: str) -> str:
    """
    This method will normalize text
    :param text:
    :rtype: str
    """
    normalize_text = text.lower()
    normalize_text = ''.join((c for c in unicodedata.normalize('NFD', normalize_text)
                        if 'Mn' != unicodedata.category(c)))

    normalize_text = re.sub('[^a-zA-Z0-9 \n\.]', ' ', normalize_text)

    return normalize_text


def get_pourcentage(int_greater: int, int_smaller: int) -> float:
    dif = int_greater - int_smaller
    result = dif/int_greater

    return result